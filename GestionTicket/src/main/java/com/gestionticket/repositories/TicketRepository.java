package com.gestionticket.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gestionticket.models.*;




@Repository
public interface TicketRepository extends JpaRepository<TicketM, Long>{
	List<TicketM> findByStatutOrderByDateDecreation(String statut);
	List<TicketM> findByTitre(String titre);
	List<TicketM> findByAssigner(String assigner);
	List<TicketM> findByLabel(label label);
	List<TicketM> findByAssignerAndStatut(String assigner, String statut);
	List<TicketM> findByLabelAndStatut(label label, String statut);
	List<TicketM> findByUserIdOrderByDateDecreation(Long id);
	List<TicketM> findByUserIdAndStatut(Long id,String statut);
	List<TicketM> findByUserIdAndLabel(Long id,label label);
	@Query("SELECT count(*) FROM ticket t where t.statut='Fermé' ")
    float ticketstatutFerme();
	@Query("SELECT count(*) FROM ticket t where t.statut='En Attente' ")
    float ticketstatutEnAttente();
	@Query("SELECT count(*) FROM ticket t where t.statut='En Cour' ")
    float ticketstatutEnCour();
	@Query("SELECT count(*) FROM ticket t where t.statut='En attente de confirmation' ")
    float ticketstatutEnAttenteDeConfirmation();	
	@Query("SELECT count(*) FROM ticket t where t.label.name='rh' ")
    float ticketlabelrh();
	@Query("SELECT count(*) FROM ticket t where t.label.name='mission' ")
    float ticketlabelmission();
	@Query("SELECT count(*) FROM ticket t where t.label.name='facturation' ")
    float ticketlabelfaccturation();
}
