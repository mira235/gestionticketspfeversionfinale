package com.example.demo.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("user-server")
public interface FeignInterface {
	

    @GetMapping("/api/StatutEnattente/{titre}")
    public void SetStatutTicketEnatt (@PathVariable String titre) ;

}
